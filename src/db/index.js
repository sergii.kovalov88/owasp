import firebase from "firebase/app";
import "firebase/firestore";

const db = firebase
  .initializeApp({
    apiKey: "AIzaSyAcOvRWUKc1_YMFPaERDfZl8Yq6q71ei34",
    authDomain: "servicario-cb872.firebaseapp.com",
    projectId: "servicario-cb872",
    storageBucket: "servicario-cb872.appspot.com",
    messagingSenderId: "1016616983995",
    appId: "1:1016616983995:web:f3a0293a0e9a6897c7789e"
  })
  .firestore();

export default db;

const { Timestamp } = firebase.firestore;

export { Timestamp };
