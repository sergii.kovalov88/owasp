/* eslint jsx-a11y/anchor-is-valid: 0 */

import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { ADMIN } from "types";

const Navbar = props => {
  const { user, isAuth } = props.auth;
  const { logout } = props;

  useEffect(() => {
    const script = document.createElement("script");
    script.src = `${process.env.PUBLIC_URL}/js/fresh.js`;
    script.async = true;
    document.body.appendChild(script);
  }, []);
  return (
    <nav
      id={props.id || ""}
      className="navbar is-fresh is-transparent no-shadow"
      role="navigation"
      aria-label="main navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <Link className="navbar-item" to="/">
            <div className="title">OWASP TOP 10</div>
          </Link>

          <a className="navbar-item is-hidden-desktop is-hidden-tablet sidebar-hide-mobile">
            <div
              id="menu-icon-wrapper"
              className="menu-icon-wrapper"
              style={{ visibility: "visible" }}
            >
              <svg width="1000px" height="1000px">
                <path
                  className="path1"
                  d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"
                />
                <path className="path2" d="M 300 500 L 700 500" />
                <path
                  className="path3"
                  d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"
                />
              </svg>
              <button id="menu-icon-trigger" className="menu-icon-trigger" />
            </div>
          </a>

          <a
            role="button"
            className="navbar-burger"
            aria-label="menu"
            aria-expanded="false"
            data-target="navbar-menu"
          >
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>

        <div id="navbar-menu" className="navbar-menu is-static">
          <div className="navbar-start">
            <a className="navbar-item is-hidden-mobile sidebar-hide-mobile">
              <div
                id="menu-icon-wrapper"
                className="menu-icon-wrapper"
                style={{ visibility: "visible" }}
              >
                <svg width="1000px" height="1000px">
                  <path
                    className="path1"
                    d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"
                  />
                  <path className="path2" d="M 300 500 L 700 500" />
                  <path
                    className="path3"
                    d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"
                  />
                </svg>
                <button id="menu-icon-trigger" className="menu-icon-trigger" />
              </div>
            </a>
          </div>

          <div className="navbar-end">
            {user && (
              <div className="navbar-item is-secondary user-welcome">
                {`Hi ${user.firstName} ${user.lastName}`}
              </div>
            )}

            <Link to="/" className="navbar-item is-secondary">
              Homepage
            </Link>

            {user && (
              <Link to="/personal" className="navbar-item is-secondary">
                Personal
              </Link>
            )}
            {user &&
              user.role === ADMIN && (
                <Link to="/admin" className="navbar-item is-secondary">
                  Admin
                </Link>
              )}
            {user &&
              user.role === ADMIN && (
                <Link to="/post" className="navbar-item is-secondary">
                  Post
                </Link>
              )}

            {!isAuth && (
              <React.Fragment>
                <Link
                  to="/login"
                  className="navbar-item is-secondary modal-trigger"
                  data-modal="auth-modal"
                >
                  <span className="button signup-button btnLogin rounded raised">
                    Anmelden
                  </span>
                </Link>

                <Link className="navbar-item" to="/register">
                  <span className="button signup-button rounded btnLogin raised">
                    Registrieren
                  </span>
                </Link>
              </React.Fragment>
            )}
            {isAuth && (
              <div onClick={logout} className="navbar-item" to="/">
                <span className="button signup-button btnLogout rounded raised">
                  Abmelden
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
