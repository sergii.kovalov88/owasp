/* eslint jsx-a11y/anchor-is-valid: 0 */
import React from "react";

const Hero = () => {
  return (
    <section className="hero is-default is-bold">
      <div className="hero-body">
        <div className="container">
          <div className="columns is-vcentered">
            <div className="column is-5 is-offset-1 landing-caption">
              <h1 className="title is-3 is-bold is-spaced">
                A4 - XML External Entities (XXE)
              </h1>
              <h1 className="title is-3 is-bold is-spaced">
                A5: Broken Access Control
              </h1>
            </div>
            <div className="column is-5 is-offset-1">
              <figure className="image is-4by3">
                <img
                  src="https://3cdhsv4crsio1kakza23qz4h-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/PNG_OWASP-AppSec.png"
                  alt="Description"
                  style={{
                    width: "50rem",
                    height: "35rem",
                    marginTop: "-5rem"
                  }}
                />
              </figure>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
