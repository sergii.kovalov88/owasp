/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import { Link } from "react-router-dom";
import { ADMIN } from "types";

const Sidebar = props => {
  const { user } = props.auth;
  return (
    <div className="sidebar">
      <div className="sidebar-header">
        <div className="sidebar-title">Menu</div>
        <a className="sidebar-close">
          <i className="fa fa-times-circle" style={{ color: `#fdd62f` }} />
        </a>
      </div>
      <div className="inner">
        <ul className="sidebar-menu">
          <li>
            <span className="nav-section-title" />
          </li>

          <li className="sidebar-close">
            <Link to="/">Homepage</Link>
          </li>

          <li className="sidebar-close">
            {user && <Link to="/personal">Personal</Link>}
          </li>

          <li className="sidebar-close">
            {user && user.role === ADMIN && <Link to="/admin">Admin</Link>}
          </li>
          <li className="sidebar-close">
            {user &&
              user.role === ADMIN && (
                <Link to="/post" className="navbar-item-side">
                  Post
                </Link>
              )}
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
