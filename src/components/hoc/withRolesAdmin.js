import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { ADMIN } from "types";

const withRolesAdmin = Component => {
  class WithRolesAdmin extends React.Component {
    render() {
      const { auth } = this.props;
      return auth.isAuth && auth.user.role === ADMIN ? (
        <Component {...this.props} />
      ) : (
        <section className="section is-medium">
          <div className="container">
            <h1 style={{ color: "red" }}>You have no access to this page </h1>
          </div>
        </section>
      );
    }
  }

  const mapStateToProps = state => ({ auth: state.auth });
  return connect(mapStateToProps)(WithRolesAdmin);
};

export default withRolesAdmin;
