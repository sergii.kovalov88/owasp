import { SET_AUTH_USER, RESET_AUTH_STATE } from "types";

import * as api from "api";

export const selfRegister = registerFormData =>
  api.selfRegister({ ...registerFormData });

export const register = registerFormData => api.register({ registerFormData });

export const login = loginFormData => api.login({ ...loginFormData });
export const resetPassword = resetFormData =>
  api.resetPassword({ ...resetFormData });

export const logout = () => dispatch =>
  api.logout().then(_ => dispatch({ user: null, type: SET_AUTH_USER }));

export const onAuthStateChanged = onAuthCallback =>
  api.onAuthStateChanged(onAuthCallback);

export const storeAuthUser = authUser => dispatch => {
  if (authUser) {
    return api
      .getUserProfile(authUser.email, authUser.uid)
      .then(userWithProfile =>
        dispatch({ user: userWithProfile, type: SET_AUTH_USER })
      );
  } else {
    return dispatch({ user: null, type: SET_AUTH_USER });
  }
};

export const resetAuthState = () => ({
  type: RESET_AUTH_STATE
});
