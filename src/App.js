import React from "react";
import { Provider } from "react-redux";
import initStore from "./store";
import { BrowserRouter as Router } from "react-router-dom";
import { ToastProvider } from "react-toast-notifications";
import ServiceApp from "./ServiceApp";
import {
  onAuthStateChanged,
  storeAuthUser,
  resetAuthState,
  logout
} from "actions";

const store = initStore();

class App extends React.Component {
  handleLogout = () => store.dispatch(logout());

  componentDidMount() {
    this.unsubscribeAuth = onAuthStateChanged(authUser => {
      store.dispatch(resetAuthState());
      store.dispatch(storeAuthUser(authUser));
    });
  }

  componentWillUnmount() {
    this.unsubscribeAuth();
    this.handleLogout();
  }

  render() {
    return (
      <Provider store={store}>
        <ToastProvider>
          <Router>
            <ServiceApp />
          </Router>
        </ToastProvider>
      </Provider>
    );
  }
}

export default App;
