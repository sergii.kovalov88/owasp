import { createStore, applyMiddleware, compose } from "redux";
import serviceApp from "reducers";

import thunk from "redux-thunk";
import logger from "redux-logger";

const initStore = () => {
  const middlewares = [thunk];

  const comsopeEnhancees =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  //middlewares.push(logger);

  const store = createStore(
    serviceApp,
    applyMiddleware(...middlewares)
    //comsopeEnhancees(applyMiddleware(...middlewares))
  );

  return store;
};

export default initStore;
