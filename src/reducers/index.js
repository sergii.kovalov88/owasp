import { combineReducers } from "redux";
import auth from "./auth";

const serviceApp = combineReducers({
  auth
});

export default serviceApp;
