import db from "db";
import firebase from "firebase/app";
import "firebase/auth";

//------------------AUTH START --------------------//
const createUserProfile = userProfile =>
  db
    .collection("users")
    .doc(userProfile.email)
    .set(userProfile);

export const selfRegister = async ({ email, password }) => {
  try {
    await firebase.auth().createUserWithEmailAndPassword(email, password);
  } catch (error) {
    const errorMessage = error.message;
    return Promise.reject(errorMessage);
  }
};

export const register = async userData => {
  try {
    userData.registerFormData.registrationDate = firebase.firestore.Timestamp.now();

    const userProfile = {
      ...userData.registerFormData
    };
    await createUserProfile(userProfile);
    return userProfile;
  } catch (error) {
    const errorMessage = error.message;
    return Promise.reject(errorMessage);
  }
};

export const login = ({ email, password }) => {
  return firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .catch(error => Promise.reject(error.message));
};

export const resetPassword = ({ email }) => {
  return firebase.auth().sendPasswordResetEmail(email);
};

export const logout = () => firebase.auth().signOut();

export const onAuthStateChanged = onAuthCallback => {
  return firebase.auth().onAuthStateChanged(onAuthCallback);
};

export const getUserProfile = (email, uid) =>
  db
    .collection("users")
    .doc(email)
    .get()
    .then(snapshot => ({ uid, ...snapshot.data() }));

//------------------AUTH STOP --------------------//
