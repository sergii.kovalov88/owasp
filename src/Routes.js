import React from "react";
import { Switch, Route } from "react-router-dom";
import HomePage from "./pages/Home";
import PostPage from "pages/post";
import LoginPage from "pages/Login";
import RegisterPage from "pages/Register";
import ForgotPassword from "pages/ForgotPassword";
import AdminPage from "pages/SecretPage";
import PersonalPage from "pages/Personal";

const Routes = () => {
  return (
    <Switch>
      <Route path="/login">
        <LoginPage />
      </Route>
      <Route path="/register">
        <RegisterPage />
      </Route>
      <Route path="/post">
        <PostPage />
      </Route>
      <Route path="/forgot-password">
        <ForgotPassword />
      </Route>
      <Route path="/admin">
        <AdminPage />
      </Route>
      <Route path="/personal">
        <PersonalPage />
      </Route>
      <Route path="/">
        <HomePage />
      </Route>
    </Switch>
  );
};

export default Routes;
