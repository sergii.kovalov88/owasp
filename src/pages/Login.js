/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { login } from "actions";
import { useToasts } from "react-toast-notifications";
import { Redirect } from "react-router-dom";
import onlyGuest from "components/hoc/onlyGuest";
import { Link } from "react-router-dom";

const Login = () => {
  const { register, handleSubmit } = useForm();
  const { addToast } = useToasts();
  const [redirect, setRedirect] = useState(false);

  const onLogin = async loginFormData => {
    login(loginFormData).then(
      _ => setRedirect(true),
      errorMessage => {
        addToast(errorMessage, {
          appearance: "error",
          autoDismiss: true,
          autoDismissTimeout: 3000
        });
      }
    );
  };

  if (redirect) {
    return <Redirect to="/" />;
  }

  return (
    <div className="auth-page">
      <div className="container has-text-centered">
        <div className="column is-4 is-offset-4">
          <h3 className="title has-text-grey">Anmeldung</h3>

          <div className="box">
            <figure className="avatar">
              <img
                className="imageLogoLogin"
                src="https://3cdhsv4crsio1kakza23qz4h-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/PNG_OWASP-AppSec.png"
                alt="Company Logo"
              />
            </figure>
            <form onSubmit={handleSubmit(onLogin)}>
              <div className="field">
                <div className="control">
                  <input
                    ref={register}
                    name="email"
                    className="input is-large"
                    type="email"
                    placeholder="Your Email"
                    autoComplete="email"
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <input
                    ref={register}
                    name="password"
                    className="input is-large"
                    type="password"
                    placeholder="Your Password"
                    autoComplete="current-password"
                  />
                </div>
              </div>
              <button
                type="submit"
                className="button is-block btnLogin is-large is-fullwidth"
              >
                Anmelden
              </button>
              <p style={{ textAlign: "center", marginTop: "1rem" }}>
                <Link to="/forgot-password">Passwort vergessen?</Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default onlyGuest(Login);
