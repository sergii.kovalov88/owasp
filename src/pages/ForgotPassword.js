/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { resetPassword } from "actions";
import { useToasts } from "react-toast-notifications";
import { Redirect } from "react-router-dom";
import onlyGuest from "components/hoc/onlyGuest";

const ForgotPassword = () => {
  const { register, handleSubmit, errors, getValues } = useForm();
  const { addToast } = useToasts();
  const [redirect, setRedirect] = useState(false);

  const resetPass = resetFormData => {
    resetPassword(resetFormData).then(
      _ => setRedirect(true),
      errorMessage => {
        console.log(errorMessage);
        if (errorMessage.code === "auth/user-not-found") {
          addToast("Email-Adresse wurde nicht gefunden", {
            appearance: "error",
            autoDismiss: true,
            autoDismissTimeout: 3000
          });
        }
      }
    );
  };

  if (redirect) {
    return <Redirect to="/reset-password-landing" />;
  }

  return (
    <div className="auth-page">
      <div className="container has-text-centered">
        <div className="column is-4 is-offset-4">
          <h3 className="title has-text-grey">Passwort vergessen?</h3>

          <div className="box">
            <figure className="avatar">
              <img
                className="imageLogoLogin"
                src={process.env.PUBLIC_URL + "/logo.svg"}
                alt="Company Logo"
              />
            </figure>
            <form onSubmit={handleSubmit(resetPass)}>
              <div className="field">
                <div className="control">
                  <input
                    ref={register({
                      required: true,
                      pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    })}
                    name="email"
                    className="input is-large"
                    type="email"
                    placeholder="Your Email"
                    autoComplete="email"
                  />
                  {errors.email && (
                    <div className="form-error">
                      {errors.email.type === "required" && (
                        <span className="help is-danger">
                          Email is required
                        </span>
                      )}

                      {errors.email.type === "pattern" && (
                        <span className="help is-danger">
                          Email address is not valid
                        </span>
                      )}
                    </div>
                  )}
                </div>
              </div>

              <button
                type="submit"
                className="button is-block btnLogin is-large is-fullwidth"
              >
                Absenden
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default onlyGuest(ForgotPassword);
