import React from "react";
import withAuthorization from "components/hoc/withAuthorization";

const PersonalPage = () => {
  return (
    <section className="section is-medium">
      <div className="container">
        <h1>This page can only be visible for the Authentificated users </h1>
      </div>
    </section>
  );
};
//export default PersonalPage;
export default withAuthorization(PersonalPage);
