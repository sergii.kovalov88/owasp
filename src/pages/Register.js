/* eslint jsx-a11y/anchor-is-valid: 0 */

import React, { useState } from "react";
import RegisterFormSelf from "components/auth/RegisterFormSelf";
import { selfRegister, register } from "actions";
import { useToasts } from "react-toast-notifications";
import { Redirect } from "react-router-dom";
import onlyGuest from "components/hoc/onlyGuest";

const Register = props => {
  const { addToast } = useToasts();
  const [redirect, setRedirect] = useState(false);

  const registerUser = async userData => {
    selfRegister(userData).then(
      _ => {
        register(userData).then(
          _ => {
            setRedirect(true);
          },
          errorMessage => {
            addToast(errorMessage, {
              appearance: "error",
              autoDismiss: true,
              autoDismissTimeout: 3000
            });
          }
        );
      },
      errorMessage => {
        addToast(errorMessage, {
          appearance: "error",
          autoDismiss: true,
          autoDismissTimeout: 3000
        });
      }
    );
  };

  if (redirect) {
    return <Redirect to="/" />;
  }

  return (
    <div className="auth-page">
      <div className="container has-text-centered">
        <div className="column is-6 is-offset-3">
          <h3 className="title has-text-grey">Neuen Benutzer Registrieren</h3>

          <div className="box">
            <figure className="avatar">
              <img
                className="imageLogoLogin"
                src="https://3cdhsv4crsio1kakza23qz4h-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/PNG_OWASP-AppSec.png"
                alt="Logo"
              />
            </figure>
          </div>
          <RegisterFormSelf onRegister={registerUser} />
        </div>
      </div>
    </div>
  );
};

export default onlyGuest(Register);
