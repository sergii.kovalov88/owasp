import React from "react";
import { useForm } from "react-hook-form";
import withRolesAdmin from "components/hoc/withRolesAdmin";

const Post = () => {
  const { register, handleSubmit } = useForm();
  const onPost = postFormData => {
    console.log(postFormData);
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
      body: JSON.stringify(postFormData)
    };

    console.log(options);

    fetch("http://127.0.0.1:5000/api/XMLProcessing", options)
      .then(resp => resp.blob())
      .catch(err => {
        console.log("Error: " + err);
      });
  };

  return (
    <div className="auth-page">
      <div className="container has-text-centered">
        <div className="column is-4 is-offset-4">
          <h3 className="title has-text-grey">Post Request</h3>

          <div className="box">
            <figure className="avatar">
              <img
                className="imageLogoLogin"
                src="https://3cdhsv4crsio1kakza23qz4h-wpengine.netdna-ssl.com/wp-content/uploads/2019/10/PNG_OWASP-AppSec.png"
                alt="Company Logo"
              />
            </figure>

            <form onSubmit={handleSubmit(onPost)}>
              {/* TEXTAREA */}
              {/*
              <div className="field">
                <div className="control">
                  <textarea
                    style={{ minHeight: "10rem" }}
                    ref={register}
                    name="body"
                    className="input is-small"
                    placeholder="Body"
                    rows={55}
                  />
                </div>
              </div>
              */}

              {/* SELECT OPTIONS */}
              {/* */}
              <div className="field">
                <div className="control">
                  <label>Data Index on Server</label>
                  <select
                    name="data_index_on_server"
                    ref={register}
                    className="input is-large"
                  >
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>

                  <label> Data Index on Server </label>
                  <select
                    name="algorithm_version"
                    ref={register}
                    className="input is-large"
                  >
                    <option value="6.9.0">6.9.0</option>
                    <option value="7.0.1">7.0.1</option>
                    <option value="7.1.1">7.2.1</option>
                  </select>

                  <label>Speed</label>
                  <select
                    name="speed"
                    ref={register}
                    className="input is-large"
                  >
                    <option value="fast">fast</option>
                    <option value="slow">slow</option>
                  </select>
                </div>
              </div>

              <button
                type="submit"
                className="button is-block btnLogin is-large is-fullwidth"
              >
                Request schicken
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

//export default Post;
export default withRolesAdmin(Post);
