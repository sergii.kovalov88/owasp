import React from "react";
import withRolesAdmin from "components/hoc/withRolesAdmin";

const AdminPage = () => {
  return (
    <section className="section is-medium">
      <div className="container">
        <h1>I am some secret page that contains sensitive information </h1>
      </div>
    </section>
  );
};
//export default AdminPage;
export default withRolesAdmin(AdminPage);
