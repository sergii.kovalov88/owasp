/* eslint jsx-a11y/anchor-is-valid: 0 */

import React from "react";
import { connect } from "react-redux"; //High order component -> HOC
import Hero from "components/Hero";

class Home extends React.Component {
  render() {
    return (
      <div>
        <Hero />
        <section className="section section-feature-grey is-medium">
          <div className="container">
            <div className="title-wrapper has-text-centered">
              <h2 className="title is-2">Great Power Comes </h2>
              <h3 className="subtitle is-5 is-muted">
                With great Responsability
              </h3>
              <div className="divider is-centered" />
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Home;
